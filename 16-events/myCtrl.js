var app = angular.module('myApp', []);

app.controller('mouseCtrl', function ($scope) {
    $scope.count = 0;
});

app.controller('clickCtrl', function ($scope) {
    $scope.count = 0;
    $scope.count_2 = 0;
    $scope.increment = function() {
        $scope.count_2++;
    };
})

app.controller('toggleCtrl', function ($scope) {
    $scope.showme = false;
    $scope.toggle = function () {
        $scope.showme = !$scope.showme;
    };
});

app.controller('eventCtrl', function ($scope) {
    $scope.left = document.getElementById("dam").offsetLeft;
    $scope.top = document.getElementById("dam").offsetTop;

    $scope.getCoordinates = function (ev) {
        $scope.x = ev.pageX - $scope.left;
        $scope.y = ev.pageY - $scope.top;
    }
});