var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
    $http.get('welcome.htm')
        .then(function (response) {
            $scope.myWelcome = response.data;
        });
});

app.controller('complicatedCtrl', function ($scope, $http, $interval) {
    $scope.welcome = "welcome.htm"
    $interval(function() {
        $http({
            method : "GET",
            url : $scope.welcome
        }).then(function mySuccess(response) {
            $scope.myWelcome = response.data;
        }, function myError(response) {
            $scope.myWelcome = response.status + " " + response.statusText;
        });
    }, 50);

});

app.controller('jsonCtrl', function ($scope, $http) {
    $http.get("customers.json")
        .then(function(response) {
            $scope.myData = response.data.records;
        });
});