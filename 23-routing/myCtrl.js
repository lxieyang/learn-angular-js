var app = angular.module("myApp", ["ngRoute"]);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "home.htm"
        })
        .when("/london", {
            templateUrl : "london.htm",
            controller : "londonCtrl"
        })
        .when("/paris", {
            templateUrl : "paris.htm",
            controller : "parisCtrl"
        })
        .when("/banana", {
            template : "<h1>Banana</h1><p>Bananas contain around 75% water.</p>"
        })
        .otherwise({redirectTo:'/'});
}]);

app.controller("londonCtrl", function ($scope) {
    $scope.msg = "I hate London";
});

app.controller("parisCtrl", function ($scope) {
    $scope.msg = "I hate Paris";
});