var app = angular.module('myApp', []);

app.controller('urlCtrl', function ($scope, $location) {
    $scope.myurl = $location.absUrl();
});

app.controller('timeoutCtrl', function ($scope, $timeout) {
   $scope.header = "Hello World!";
   $timeout(function () {
       $scope.header = "I wanna kill you!";
   }, 3000);
});

app.controller('intervalCtrl', function ($scope, $interval) {
    $scope.currTime = new Date().toLocaleTimeString();
    $interval(function () {
        $scope.currTime = new Date().toLocaleTimeString();
    }, 1000);
});

app.service('hexafy', function () {
    this.myFunc = function (x) {
        return x.toString(16);
    }
});

app.controller('customCtrl', function ($scope, $interval, hexafy) {
    $scope.val = 255;
    $scope.val_2 = 255;
    $interval(function () {
        $scope.hex = hexafy.myFunc($scope.val);
    }, 50);
});

app.filter('myFormat', ['hexafy', function (hexafy) {
    return function(x) {
        return hexafy.myFunc(x);
    }
}]);