var app = angular.module('myApp', []);

app.controller('caseCtrl', function ($scope) {
    $scope.x1 = "JOHN";
    $scope.x2 = angular.lowercase($scope.x1);
    $scope.ha = "michael";
    $scope.hahaha = angular.uppercase($scope.ha);
});

app.controller('stringNumberCtrl', function ($scope) {
    $scope.x1 = "John";
    $scope.x2 = angular.isString($scope.x1);
    $scope.x22 = angular.isNumber($scope.x1);
    $scope.x3 = 56;
    $scope.x4 = angular.isNumber($scope.x3);
    $scope.x44 = angular.isString($scope.x3);
});