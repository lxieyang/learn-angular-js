var app = angular.module('myApp', []);

app.controller('inputCtrl', function($scope) {
    $scope.firstName = 'Michael';
});

app.controller('checkboxCtrl', function ($scope) {
    $scope.myVal = false;
});

app.controller('radioCtrl', function ($scope) {
    $scope.myChoice = 'Michael Scofield';
});

app.controller('selectCtrl', function ($scope) {
    $scope.myChoice = '';
});

app.controller('myAppCtrl', function ($scope) {
    $scope.master = {firstName: "Michael", lastName: "Scofield"};
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    }
    $scope.reset();
});