var app = angular.module('myApp', []);

app.controller('requiredCtrl', function($scope) {

});

app.controller('emailCtrl', function ($scope) {
    
});

app.directive('myDirective', function () {
    return {
        require : 'ngModel',
        link : function(scope, element, attr, mCtrl) {
            function myValidation(value) {
                if (value.indexOf("e") > -1) {
                    mCtrl.$setValidity('charE', true);
                } else {
                    mCtrl.$setValidity('charE', false);
                }
                return value;
            }
            mCtrl.$parsers.push(myValidation);
        }
    };
});

app.controller('appCtrl', function ($scope) {
    $scope.user = 'Michael Scofield';
    $scope.email = 'lxieyang@umich.edu';
});